package com.mycalendar.martha.Task;

import com.mycalendar.model.Models.Task;

public interface OnTaskClickListener {
    void onTaskClicked(Task task);
}
