package com.mycalendar.martha.Task;

import com.mycalendar.model.Models.Task;

import java.util.ArrayList;

public interface TasksFetchListener {
    void onResponse(ArrayList<Task> tasks);
}
