package com.mycalendar.martha.Task;

public interface TaskRequestListener {
    void onResponse(boolean success);
}
