package com.mycalendar.martha.Class;

import com.mycalendar.model.Models.Class;

public interface OnClassClickListener {
    void onClassClicked(Class Class);
}
