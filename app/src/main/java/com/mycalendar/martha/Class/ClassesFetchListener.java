package com.mycalendar.martha.Class;

import com.mycalendar.model.Models.Class;

import java.util.ArrayList;

public interface ClassesFetchListener {
    void onResponse(ArrayList<Class> classes);
}
