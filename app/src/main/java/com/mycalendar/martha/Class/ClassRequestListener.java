package com.mycalendar.martha.Class;

public interface ClassRequestListener {
    void onResponse(boolean success);
}
