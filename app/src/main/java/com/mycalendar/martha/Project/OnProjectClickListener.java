package com.mycalendar.martha.Project;

import com.mycalendar.model.Models.Project;

public interface OnProjectClickListener {
    void onProjectClicked(Project project);
}
