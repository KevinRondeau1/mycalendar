package com.mycalendar.martha.Project;

public interface ProjectRequestListener {
    void onResponse(boolean success);
}
