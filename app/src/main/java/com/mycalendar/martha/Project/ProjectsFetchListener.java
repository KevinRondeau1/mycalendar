package com.mycalendar.martha.Project;

import com.mycalendar.model.Models.Project;

import java.util.ArrayList;

public interface ProjectsFetchListener {
    void onResponse(ArrayList<Project> projects);
}
