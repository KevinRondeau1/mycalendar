package com.mycalendar.martha.Meeting;

import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Project;

public interface OnMeetingClickListener {
    void onMeetingClicked(Meeting meeting);
}
