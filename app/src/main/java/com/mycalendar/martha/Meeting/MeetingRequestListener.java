package com.mycalendar.martha.Meeting;

public interface MeetingRequestListener {
    void onResponse(boolean success);
}
