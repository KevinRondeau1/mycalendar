package com.mycalendar.martha.Meeting;

import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Project;

import java.util.ArrayList;

public interface MeetingsFetchListener {
    void onResponse(ArrayList<Meeting> meetings);
}
