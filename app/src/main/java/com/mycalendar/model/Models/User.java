package com.mycalendar.model.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class User implements Parcelable{
    private int id;
    private String eMail;
    private String firstName;
    private String lastName;
    private String password;

    public User(JSONObject userJson) throws JSONException {
        this.id=userJson.getInt("id");
        this.eMail=userJson.getString("email");
    }

    public int getId() {
        return id;
    }

    public User(int id,String email, String firstname,String lastname, String password) {
        this.id = id;
        this.eMail = email;
        this.firstName = firstname;
        this.lastName = lastname;
        this.password = password;
    }

    protected User(Parcel in) {
        id = in.readInt();
        eMail = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(eMail);
        parcel.writeInt(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstname='" + firstName + '\'' +
                ", lastname='" + lastName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
