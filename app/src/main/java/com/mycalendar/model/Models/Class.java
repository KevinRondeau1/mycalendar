package com.mycalendar.model.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Class implements Parcelable{
    private int id;
    private String className;
    private String teacherName;

    public Class(JSONObject tasksJson) throws JSONException {
        this.id = tasksJson.getInt("id");
        this.className = tasksJson.getString("ClassName");
        this.teacherName = tasksJson.getString("TeacherName");
    }

    public int getId() {
        return id;
    }
    public String getClassName() {return className;}
    public String getTeacherName() { return teacherName; }

    public Class(int id, String classname, String teachername) {
        this.id = id;
        this.className = classname;
        this.teacherName = teachername;
    }

    // Currently not used
    public void update(int id, String classname, String teachername) {
        this.id = id;
        this.className = classname;
        this.teacherName = teachername;
    }

    protected Class(Parcel in) {
        id = in.readInt();
        className = in.readString();
        teacherName = in.readString();
    }

    public static final Parcelable.Creator<Class> CREATOR = new Parcelable.Creator<Class>() {
        @Override
        public Class createFromParcel(Parcel in) {
            return new Class(in);
        }

        @Override
        public Class[] newArray(int size) {
            return new Class[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(className);
        parcel.writeString(teacherName);
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", classname='" + className + '\'' +
                ", teachername='" + teacherName + '\'' +
                '}';
    }
}
