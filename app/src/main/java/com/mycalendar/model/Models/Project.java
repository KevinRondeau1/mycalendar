package com.mycalendar.model.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mycalendar.model.Services.ProjectService;

import org.json.JSONException;
import org.json.JSONObject;

public class Project implements Parcelable {
    private int id;
    private String projectName;
    private String teamMembers;
    private String note;
    private String dueDate;

    public Project(JSONObject projectsJson) throws JSONException {
        this.id = projectsJson.getInt("id");
        this.projectName = projectsJson.getString("ProjectName");
        this.teamMembers = projectsJson.getString("TeamMembers");
        this.note = projectsJson.getString("Note");
        this.dueDate = projectsJson.getString("DueDate");
    }

    public int getId() { return id;  }
    public String getProjectName() {return projectName;}
    public String getTeamMembers() { return teamMembers; }
    public String getNote() { return note; }
    public String getDueDate() { return dueDate; }


    public Project(int id, String projectName, String teamMembers, String note, String dueDate) {
        this.id = id;
        this.projectName = projectName;
        this.teamMembers = teamMembers;
        this.note = note;
        this.dueDate=dueDate;
    }

    // Used in ProjectService to edit a project and keep the currentProject updated ("on our side")
    public void update(int id, String projectName, String teamMembers, String note, String dueDate) {
        this.id = id;
        this.projectName = projectName;
        this.teamMembers = teamMembers;
        this.note = note;
        this.dueDate=dueDate;
    }

    protected Project(Parcel in) {
        id = in.readInt();
        projectName = in.readString();
        teamMembers = in.readString();
        note = in.readString();
        dueDate = in.readString();
    }

    public static final Creator<Project> CREATOR = new Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(projectName);
        parcel.writeString(teamMembers);
        parcel.writeString(note);
        parcel.writeString(dueDate);
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", projectName='" + projectName + '\'' +
                ", teamMembers='" + teamMembers + '\'' +
                ", note=" + note +
                ", dueDate=" + dueDate +
                '}';
    }
}
