package com.mycalendar.model.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Meeting implements Parcelable {
    private int id;
    private String place;
    private String meetDate;
    private int meetHour;
    private int meetMinute;
    private int durationHours;
    private int durationMinutes;
    private String attendances;
    private int projectId;

    public Meeting(JSONObject meetingsJson) throws JSONException {
        this.id = meetingsJson.getInt("id");
        this.place = meetingsJson.getString("Place");
        this.meetDate = meetingsJson.getString("MeetDate");
        this.meetHour = meetingsJson.getInt("MeetHour");
        this.meetMinute = meetingsJson.getInt("MeetMinute");
        this.durationHours = meetingsJson.getInt("DurationHours");
        this.durationMinutes = meetingsJson.getInt("DurationMinutes");
        this.attendances = meetingsJson.getString("Attendances");
        this.projectId = meetingsJson.getInt("project_id");
    }

    public int getId() { return id;  }
    public String getPlace() {return place;}
    public String getMeetDate() { return meetDate; }
    public int getMeetHour() { return meetHour; }
    public int getMeetMinute() { return meetMinute; }
    public int getDurationHours() { return durationHours; }
    public int getDurationMinutes() { return durationMinutes; }
    public String getAttendances() { return attendances; }
    public int getProjectId() { return projectId;  }
    public String getMeetTimeFormatted(){
        String temp;
        if(meetMinute >= 0 && meetMinute < 10) temp = "0" + meetMinute;
        else temp = String.valueOf(meetMinute);
        return meetHour + ":" + temp;
    }
    public String getMeetDurationFormatted(){
        return durationHours + ":" + durationMinutes;
    }


    public Meeting(int id, String place, String meetDate, int meetHour, int meetMinute, int durationHours, int durationMinutes, String attendances, int projectId) {
        this.id = id;
        this.place = place;
        this.meetDate = meetDate;
        this.meetHour = meetHour;
        this.meetMinute = meetMinute;
        this.durationHours = durationHours;
        this.durationMinutes = durationMinutes;
        this.attendances = attendances;
        this.projectId = projectId;
    }

    // Currently not used
    public void update(int id, String place, String meetDate, int meetHour, int meetMinute, int durationHours, int durationMinutes, String attendances) {
        this.id = id;
        this.place = place;
        this.meetDate = meetDate;
        this.meetHour = meetHour;
        this.meetMinute = meetMinute;
        this.durationHours = durationHours;
        this.durationMinutes = durationMinutes;
        this.attendances=attendances;
    }

    protected Meeting(Parcel in) {
        id = in.readInt();
        place = in.readString();
        meetDate = in.readString();
        meetHour = in.readInt();
        meetMinute = in.readInt();
        durationHours = in.readInt();
        durationMinutes = in.readInt();
        attendances = in.readString();
    }

    public static final Creator<Meeting> CREATOR = new Creator<Meeting>() {
        @Override
        public Meeting createFromParcel(Parcel in) {
            return new Meeting(in);
        }

        @Override
        public Meeting[] newArray(int size) {
            return new Meeting[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(place);
        parcel.writeString(meetDate);
        parcel.writeInt(meetHour);
        parcel.writeInt(meetMinute);
        parcel.writeInt(durationHours);
        parcel.writeInt(durationMinutes);
        parcel.writeString(attendances);
        parcel.writeInt(projectId);
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", place='" + place + '\'' +
                ", meetTime='" + meetDate + '\'' +
                ", meetHour=" + meetHour +
                ", meetMinute=" + meetMinute +
                ", durationHours=" + durationHours +
                ", durationMinutes=" + durationMinutes +
                ", attendances=" + attendances +
                ", attendances=" + projectId +
                '}';
    }
}
