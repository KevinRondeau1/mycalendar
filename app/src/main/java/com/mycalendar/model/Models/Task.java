package com.mycalendar.model.Models;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class Task implements Parcelable {
    private int id;
    private String name;
    private String description;
    private int completion;
    private String dueDate;
    private String state;

    public Task(JSONObject tasksJson) throws JSONException {
        this.id = tasksJson.getInt("id");
        this.name = tasksJson.getString("TaskName");
        this.description = tasksJson.getString("Descript");
        this.completion = tasksJson.getInt("CompletionProgress");
        this.dueDate = tasksJson.getString("DueDate");
        this.state = tasksJson.getString("TaskState");
    }

    public int getId() {
        return id;
    }
    public String getName() {return name;}
    public String getDescription() { return description; }
    public int getCompletion() { return completion; }
    public String getDueDate() { return dueDate; }
    public String getState() {
        return state;
    }


    public Task(int id, String name, String descript, int comp, String dDate,String State) {
        this.id = id;
        this.name = name;
        this.description = descript;
        this.completion = comp;
        this.dueDate=dDate;
        this.state = State;
    }

    // Currently not used
    public void update(int id, String name, String descript, int comp, String dDate,String State) {
        this.id = id;
        this.name = name;
        this.description = descript;
        this.completion = comp;
        this.dueDate=dDate;
        this.state = State;
    }

    protected Task(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        completion = in.readInt();
        dueDate = in.readString();
        state = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(completion);
        parcel.writeString(dueDate);
        parcel.writeString(state);
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", completion=" + completion +
                ", dueMonth=" + dueDate +
                ", state='" + state + '\'' +
                '}';
    }
}
