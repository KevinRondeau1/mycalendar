package com.mycalendar.model.Services;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mycalendar.controller.DetailProjectActivity;
import com.mycalendar.martha.MarthaQueue;
import com.mycalendar.martha.MarthaRequest;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Project.ProjectsFetchListener;
import com.mycalendar.model.Models.Project;
import com.mycalendar.model.Models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ProjectService {
    private static ProjectService instance;

    private Project currentProject;

    public Project getCurrentProject() {
        return currentProject;
    }
    public void setCurrentProject(Project currentProject) {
        this.currentProject = currentProject;
    }

    private ProjectService(){}

    public static ProjectService getInstance() {
        if (instance == null) instance = new ProjectService();
        return instance;
    }

    public void getAll(int userId, final ProjectsFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("uid", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-projects-by-user-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Project> projects = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        projects.add(new Project(data.getJSONObject(i)));
                    }

                    listener.onResponse(projects);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }

    public void edit(final int projectId, String ProjectName, String TeamMembers, String Note, String dueDate, ProjectRequestListener listener, Context context) {
        if ( isInvalid(ProjectName, TeamMembers, Note) ) listener.onResponse(false);
        else {
            JSONObject editParams = new JSONObject();
            try {
                editParams.put("id", projectId);
                buildProjectParams(editParams, ProjectName, TeamMembers, Note, dueDate);
                currentProject.update(projectId, ProjectName, TeamMembers, Note, dueDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("update-project", editParams, listener, context);
        }
    }

    public void add(final String ProjectName, final String TeamMembers, final String Note, final String dueDate, final ProjectRequestListener listener, Context context) {
        if ( isInvalid(ProjectName, TeamMembers, Note) ) listener.onResponse(false);
        else {
                JSONObject projectAddParams = new JSONObject();
                try {
                    projectAddParams.put("uid", UserService.getInstance().getCurrentUser().getId());
                    buildProjectParams(projectAddParams, ProjectName, TeamMembers, Note, dueDate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                MarthaRequest addRequest = new MarthaRequest("insert-project", projectAddParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int id = response.getInt("lastInsertId");
                            currentProject = new Project(id, ProjectName, TeamMembers, Note, dueDate);

                            boolean success = response.getBoolean("success");
                            listener.onResponse(success);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            listener.onResponse(false);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onResponse(false);
                    }
                });
                MarthaQueue.getInstance(context).send(addRequest);
        }
    }

    public void delete(final int projectId, ProjectRequestListener listener, Context context) {
            JSONObject editParams = new JSONObject();
            try {
                editParams.put("id", projectId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("delete-project-by-id", editParams, listener, context);
        }

    private void buildProjectParams(JSONObject params, String ProjectName, String TeamMembers, String Note, String dueDate) throws JSONException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = null;
        java.sql.Date sql = new java.sql.Date(System.currentTimeMillis());
        try {
            parsed = format.parse(dueDate);
            sql = new java.sql.Date(parsed.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        params.put("ProjectName", ProjectName);
        params.put("TeamMembers", TeamMembers);
        params.put("Note", Note);
        params.put("DueDate", sql);
    }

    private boolean isInvalid(String ProjectName, String TeamMembers, String Note) {
        return ProjectName.isEmpty() || TeamMembers.isEmpty() || Note.isEmpty();
    }

    private void sendRequest(String query, JSONObject params, final ProjectRequestListener listener, Context context) {
        MarthaRequest addRequest = new MarthaRequest(query, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean success = response.getBoolean("success");
                    listener.onResponse(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(false);
            }
        });
        MarthaQueue.getInstance(context).send(addRequest);
    }
}
