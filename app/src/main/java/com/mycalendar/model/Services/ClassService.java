package com.mycalendar.model.Services;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mycalendar.martha.MarthaQueue;
import com.mycalendar.martha.MarthaRequest;
import com.mycalendar.martha.Class.ClassRequestListener;
import com.mycalendar.martha.Class.ClassesFetchListener;
import com.mycalendar.martha.Task.TaskRequestListener;
import com.mycalendar.model.Models.Class;
import com.mycalendar.model.Models.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ClassService {
    private static ClassService instance;

    private Class currentClass;

    public Class getCurrentClass() {
        return currentClass;
    }
    public void setCurrentClass(Class ActualClass) {
        currentClass=ActualClass;
    }

    private ClassService(){
    }

    public static ClassService getInstance() {
        if (instance == null) {
            instance = new ClassService();
        }

        return instance;
    }

    public void getAll(int userId, final ClassesFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("uid", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-classes-by-user-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Class> classes = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        classes.add(new Class(data.getJSONObject(i)));
                    }

                    listener.onResponse(classes);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }

    private void buildClassParams(JSONObject params, String classname, String teachername) throws JSONException {
        params.put("ClassName", classname);
        params.put("TeacherName", teachername);
    }

    public void add(final String classname, final String teachername, final int userId, final ClassRequestListener listener, Context context) {
        if( isInvalid(classname, teachername) ) listener.onResponse(false);
        else {
            JSONObject addParams = new JSONObject();
            try {
                addParams.put("uid", userId);
                buildClassParams(addParams, classname, teachername);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MarthaRequest addRequest = new MarthaRequest("insert-class", addParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentClass = new Class(id, classname,teachername);

                        boolean success = response.getBoolean("success");
                        listener.onResponse(success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onResponse(false);
                }
            });
            MarthaQueue.getInstance(context).send(addRequest);
        }
    }

    public void edit(final int classId, String ClassName, String TeacherName,ClassRequestListener listener, Context context) {
        if (isInvalid(ClassName, TeacherName)) {
            listener.onResponse(false);
        } else {
            JSONObject editParams = new JSONObject();
            try {
                editParams.put("id", classId);
                buildRecipeParams(editParams, ClassName, TeacherName);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("update-class", editParams, listener, context);
        }
    }

    private void buildRecipeParams(JSONObject params, String ClassName, String TeacherName) throws JSONException {
        params.put("ClassName", ClassName);
        params.put("TeacherName", TeacherName);
    }

    private boolean isInvalid(String ClassName, String TeacherName) {
        return ClassName == null || TeacherName.isEmpty();
    }

    public void delete(int classId, ClassRequestListener listener, Context context) {
        JSONObject deleteParams = new JSONObject();
        try {
            deleteParams.put("id",classId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendRequest("delete-class-by-id", deleteParams, listener, context);
    }

    private void sendRequest(String query, JSONObject params, final ClassRequestListener listener, Context context) {
        MarthaRequest addRequest = new MarthaRequest(query, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean success = response.getBoolean("success");
                    listener.onResponse(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(false);
            }
        });
        MarthaQueue.getInstance(context).send(addRequest);
    }
}
