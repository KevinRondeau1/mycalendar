package com.mycalendar.model.Services;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mycalendar.controller.TaskActivity;
import com.mycalendar.martha.Class.ClassesFetchListener;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Task.TaskRequestListener;
import com.mycalendar.martha.MarthaQueue;
import com.mycalendar.martha.MarthaRequest;
import com.mycalendar.martha.Task.TasksFetchListener;
import com.mycalendar.model.Models.Class;
import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TaskService {
    private static TaskService instance;

    private Task currentTask;

    public Task getCurrentTask() {
        return currentTask;
    }
    public void setCurrentTask(Task ActualTask) {
         currentTask=ActualTask;
    }

    private TaskService(){
    }

    public static TaskService getInstance() {
        if (instance == null) {
            instance = new TaskService();
        }

        return instance;
    }

    public void getAll(int userId, final TasksFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("uid", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-tasks-by-user-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Task> tasks = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        tasks.add(new Task(data.getJSONObject(i)));
                    }

                    listener.onResponse(tasks);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }
    public void getClassTask(int classId, final TasksFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("cid", classId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-tasks-by-class-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Task> tasks = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        tasks.add(new Task(data.getJSONObject(i)));
                    }

                    listener.onResponse(tasks);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }
    public void getProjectTask(int projectId, final TasksFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("pid", projectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-tasks-by-project-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Task> tasks = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        tasks.add(new Task(data.getJSONObject(i)));
                    }

                    listener.onResponse(tasks);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }
   /*public void add(String category, String name, int hours, int minutes, String description, int userId, final TaskRequestListener listener, Context context) {
        if (isInvalid(category, name, hours, minutes, description)) {
            listener.onResponse(false);
        } else {
            JSONObject recipeParams = new JSONObject();
            try {
                buildRecipeParams(recipeParams, category, name, hours, minutes, description);
                recipeParams.put("user_id", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("insert-recipe", recipeParams, listener, context);
        }
    }*/


    private void buildTaskParams(JSONObject params, String Name, String Description, int completion, String state, String dueDate) throws JSONException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = null;
        java.sql.Date sql = new java.sql.Date(System.currentTimeMillis());
        try {
            parsed = format.parse(dueDate);
            sql = new java.sql.Date(parsed.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        params.put("TaskName", Name);
        params.put("Descript", Description);
        params.put("CompletionProgress", completion);
        params.put("TaskState", state);
        params.put("DueDate", sql);
    }

    private boolean isInvalid(String Name, String Description, int completion, String state,String dueDate) {
        return Name == null || Description.isEmpty()|| state.isEmpty()|| completion<0||completion>10 || dueDate.isEmpty();
    }

    public void delete(int taskId, TaskRequestListener listener, Context context) {
        JSONObject deleteParams = new JSONObject();
        try {
            deleteParams.put("id",taskId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest("delete-task-by-id", deleteParams, listener, context);
    }

    private void sendRequest(String query, JSONObject params, final TaskRequestListener listener, Context context) {
        MarthaRequest addRequest = new MarthaRequest(query, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean success = response.getBoolean("success");
                    listener.onResponse(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(false);
            }
        });
        MarthaQueue.getInstance(context).send(addRequest);
    }

    public void add(final String Title, final String descript, final int completionProgress, final String taskState, final String DueDate,final int userId, final TaskRequestListener listener, Context context) {
        if( isInvalid(Title, descript, completionProgress, taskState, DueDate) ) listener.onResponse(false);
        else {
            JSONObject addParams = new JSONObject();
            try {
                addParams.put("uid", userId);
                buildTaskParams(addParams, Title, descript, completionProgress, taskState, DueDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MarthaRequest addRequest = new MarthaRequest("insert-task", addParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentTask = new Task(id, Title, descript, completionProgress, taskState, DueDate);

                        boolean success = response.getBoolean("success");
                        listener.onResponse(success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onResponse(false);
                }
            });
            MarthaQueue.getInstance(context).send(addRequest);
        }
    }

    public void addFromClass(final String Title, final String descript, final int completionProgress, final String taskState, final String DueDate,final int userId,final int classId, final TaskRequestListener listener, Context context) {
        if( isInvalid(Title, descript, completionProgress, taskState, DueDate) ) listener.onResponse(false);
        else {
            JSONObject addParams = new JSONObject();
            try {
                addParams.put("uid", userId);
                addParams.put("cid",classId);
                buildTaskParams(addParams, Title, descript, completionProgress, taskState, DueDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MarthaRequest addRequest = new MarthaRequest("insert-task-from-class", addParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentTask = new Task(id, Title, descript, completionProgress, taskState, DueDate);

                        boolean success = response.getBoolean("success");
                        listener.onResponse(success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onResponse(false);
                }
            });
            MarthaQueue.getInstance(context).send(addRequest);
        }
    }
    public void addFromProject(final String Title, final String descript, final int completionProgress, final String taskState, final String DueDate,final int userId,final int projectId, final TaskRequestListener listener, Context context) {
        if( isInvalid(Title, descript, completionProgress, taskState, DueDate) ) listener.onResponse(false);
        else {
            JSONObject addParams = new JSONObject();
            try {
                addParams.put("uid", userId);
                addParams.put("pid",projectId);
                buildTaskParams(addParams, Title, descript, completionProgress, taskState, DueDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MarthaRequest addRequest = new MarthaRequest("insert-task-from-project", addParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentTask = new Task(id, Title, descript, completionProgress, taskState, DueDate);

                        boolean success = response.getBoolean("success");
                        listener.onResponse(success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onResponse(false);
                }
            });
            MarthaQueue.getInstance(context).send(addRequest);
        }
    }
    public void edit(final int taskId, String Name, String Description, int completion, String state, String dueDate, TaskRequestListener listener, Context context) {
        if (isInvalid(Name, Description,completion, state, dueDate)) {
            listener.onResponse(false);
        } else {

            JSONObject editParams = new JSONObject();
            try {
                editParams.put("id", taskId);
                buildTaskParams(editParams, Name, Description, completion, state, dueDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("update-task", editParams, listener, context);
        }
    }

    /* Alexis's function to force delete a project */
    public void deleteAll(final int projectId, TaskRequestListener listener, Context context) {
        JSONObject deleteAllParams = new JSONObject();
        try {
            deleteAllParams.put("pid", projectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest("delete-tasks-by-project-id", deleteAllParams, listener, context);
    }
    public void deleteAllFromClass(final int classId, TaskRequestListener listener, Context context) {
        JSONObject deleteAllParams = new JSONObject();
        try {
            deleteAllParams.put("cid", classId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest("delete-task-by-class-id", deleteAllParams, listener, context);
    }

}
