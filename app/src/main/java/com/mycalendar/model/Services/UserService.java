package com.mycalendar.model.Services;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mycalendar.martha.MarthaQueue;
import com.mycalendar.martha.MarthaRequest;
import com.mycalendar.martha.User.UserRequestListener;
import com.mycalendar.model.Models.User;

import org.json.JSONException;
import org.json.JSONObject;

public class UserService {
    private static UserService instance = null;

    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }
    /*public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }*/

    private UserService() {

        currentUser = null;

        //autoIncrement++;
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }

        return instance;
    }

    public void logIn(String email, String password, final UserRequestListener listener, Context context) {
        JSONObject loginParams = new JSONObject();
        try {
            loginParams.put("email", email);
            loginParams.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // requête vers Martha
        MarthaRequest loginRequest=new MarthaRequest("select-user-login", loginParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject userJson = response.getJSONArray("data").getJSONObject(0);

                    currentUser = new User(userJson);
                    listener.onResponse(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(false);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MYCALENDAR", "onErrorResponse: "+error.toString());
                listener.onResponse(false);
            }
        });

        MarthaQueue.getInstance(context).send(loginRequest);
    }

    public void signUp(final String email, final String firstname, final String lastname, final String password, final UserRequestListener listener, Context context) {
        if (email.isEmpty() || password.isEmpty() || firstname.isEmpty()||lastname.isEmpty()) {
            listener.onResponse(false);
        } else {
            JSONObject signupParams = new JSONObject();
            try {
                signupParams.put("email", email);
                signupParams.put("firstname", firstname);
                signupParams.put("lastname", lastname);
                signupParams.put("password", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // requête vers Martha
            MarthaRequest signupRequest = new MarthaRequest("insert-user-signup", signupParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentUser = new User(id, email, firstname, lastname, password);

                        listener.onResponse(true);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("RECIPEASY", "onErrorResponse: "+error.toString());
                    listener.onResponse(false);
                }
            });

            MarthaQueue.getInstance(context).send(signupRequest);
        }
    }

    public void logOut() {
        currentUser = null;
    }
}
