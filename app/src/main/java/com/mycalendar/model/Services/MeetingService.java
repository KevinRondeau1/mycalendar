package com.mycalendar.model.Services;

import android.content.ContentValues;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mycalendar.controller.ProjectActivity;
import com.mycalendar.martha.MarthaQueue;
import com.mycalendar.martha.MarthaRequest;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Meeting.MeetingsFetchListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Project.ProjectsFetchListener;
import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Project;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MeetingService {
    private static MeetingService instance;

    private Meeting currentMeeting;

    public Meeting getCurrentMeeting() {
        return currentMeeting;
    }
    public void setCurrentMeeting(Meeting currentMeeting) {
        this.currentMeeting = currentMeeting;
    }

    private MeetingService(){}

    public static MeetingService getInstance() {
        if (instance == null) instance = new MeetingService();
        return instance;
    }

    public void getAll(int projectId, final MeetingsFetchListener listener, Context context) {
        JSONObject fetchParams = new JSONObject();
        try {
            fetchParams.put("pid", projectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MarthaRequest fetchRequest = new MarthaRequest("select-meetings-by-project-id", fetchParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("data");
                    ArrayList<Meeting> meetings = new ArrayList<>();

                    for(int i = 0; i < data.length(); i++){
                        meetings.add(new Meeting(data.getJSONObject(i)));
                    }

                    listener.onResponse(meetings);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(null);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(null);
            }
        });

        MarthaQueue.getInstance(context).send(fetchRequest);
    }

    public void edit(final int meetingId, String place, String meetDate, int meetHour, int meetMinute, int durationHours, int durationMinutes, String attendances, MeetingRequestListener listener, Context context) {
        if( isInvalid(place, meetHour, meetMinute, durationHours, durationMinutes, attendances) ) listener.onResponse(false);
        else {
            JSONObject editParams = new JSONObject();
            try {
                editParams.put("id", meetingId);
                buildMeetingParams(editParams, place, meetDate, meetHour, meetMinute, durationHours, durationMinutes, attendances);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("update-meeting", editParams, listener, context);
        }
    }


    public void add(final int projectId, final String place, final String meetDate, final int meetHour, final int meetMinute, final int durationHours, final int durationMinutes, final String attendances, final MeetingRequestListener listener, Context context) {
        if( isInvalid(place, meetHour, meetMinute, durationHours, durationMinutes, attendances) ) listener.onResponse(false);
        else {
            JSONObject addParams = new JSONObject();
            try {
                addParams.put("pid", projectId);
                buildMeetingParams(addParams, place, meetDate, meetHour, meetMinute, durationHours, durationMinutes, attendances);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MarthaRequest addRequest = new MarthaRequest("insert-meeting", addParams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int id = response.getInt("lastInsertId");
                        currentMeeting = new Meeting(id, place, meetDate, meetHour, meetMinute, durationHours, durationMinutes, attendances, projectId);

                        boolean success = response.getBoolean("success");
                        listener.onResponse(success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        listener.onResponse(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onResponse(false);
                }
            });
            MarthaQueue.getInstance(context).send(addRequest);
        }
    }

    public void delete(final int meetingId, MeetingRequestListener listener, Context context) {
            JSONObject deleteParams = new JSONObject();
            try {
                deleteParams.put("id", meetingId);
                //buildProjectParams(editParams, Place, MeetTime, Duration, Attendances);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRequest("delete-meeting-by-id", deleteParams, listener, context);
        }

    public void deleteAll(final int projectId, MeetingRequestListener listener, Context context) {
        JSONObject deleteAllParams = new JSONObject();
        try {
            deleteAllParams.put("pid", projectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest("delete-meetings-by-project-id", deleteAllParams, listener, context);
    }

    private void buildMeetingParams(JSONObject params, String place, String meetDate, int meetHour, int meetMinute, int durationHours, int durationMinutes, String attendances) throws JSONException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed = null;
        java.sql.Date sql = new java.sql.Date(System.currentTimeMillis());
        try {
            parsed = format.parse(meetDate);
            sql = new java.sql.Date(parsed.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        params.put("Place", place);
        params.put("MeetDate", sql);
        params.put("MeetHour", meetHour);
        params.put("MeetMinute", meetMinute);
        params.put("DurationHours", durationHours);
        params.put("DurationMinutes", durationMinutes);
        params.put("Attendances", attendances);
    }

    private boolean isInvalid(String place, int meetHour, int meetMinute, int durationHours, int durationMinutes, String attendances) {
        return place.isEmpty() || attendances.isEmpty() || (meetHour == 0 && meetMinute == 0) || (durationHours == 0 && durationMinutes == 0);
    }

    private void sendRequest(String query, JSONObject params, final MeetingRequestListener listener, Context context) {
        MarthaRequest addRequest = new MarthaRequest(query, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean success = response.getBoolean("success");
                    listener.onResponse(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onResponse(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onResponse(false);
            }
        });
        MarthaQueue.getInstance(context).send(addRequest);
    }
}
