package com.mycalendar.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mycalendar.databinding.RecyclerMeetingsBinding;
import com.mycalendar.martha.Meeting.OnMeetingClickListener;
import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Project;

import java.util.ArrayList;

public class MeetingsAdapter extends RecyclerView.Adapter<MeetingsAdapter.MeetingViewHolder> {

    private final OnMeetingClickListener meetingClickListener;
    private ArrayList<Meeting> meetings;

    public void setMeetings(ArrayList<Meeting> projectMeetings) {
        this.meetings = projectMeetings;
    }
    public ArrayList<Meeting> getMeetings() {
        return meetings;
    }

    public MeetingsAdapter(OnMeetingClickListener meetingClickListener) {
        this.meetings = new ArrayList<>();
        this.meetingClickListener = meetingClickListener;
    }

    @NonNull
    @Override
    public MeetingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMeetingsBinding binding = RecyclerMeetingsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);


        return new MeetingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetingViewHolder holder, int position) {
        Meeting meeting = meetings.get(position);

        holder.bind(meeting);
    }

    @Override
    public int getItemCount() {
        return meetings.size();
    }


    public class MeetingViewHolder extends RecyclerView.ViewHolder{
        private RecyclerMeetingsBinding binding;

        public MeetingViewHolder(@NonNull RecyclerMeetingsBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bind(final Meeting meeting) {

            binding.RecyclerMeetingPlace.setText(meeting.getPlace());
            binding.RecyclerMeetingMeetTime.setText(meeting.getMeetDate() + " at " + meeting.getMeetTimeFormatted());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    meetingClickListener.onMeetingClicked(meeting);
                }
            });
        }
    }
}