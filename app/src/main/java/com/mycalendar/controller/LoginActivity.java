package com.mycalendar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mycalendar.controller.tabs.ClassesTabActivity;
import com.mycalendar.controller.tabs.TaskTabActivity;
import com.mycalendar.databinding.ActivityLoginBinding;
import com.mycalendar.martha.User.UserRequestListener;
import com.mycalendar.model.Services.UserService;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

            binding.LoginLoginButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    String email = binding.LoginEmail.getText().toString();
                    String password = binding.LoginPassword.getText().toString();

                    UserService.getInstance().logIn(email, password, new UserRequestListener() {
                        @Override
                        public void onResponse(boolean success) {
                            if(success){
                                loggedIn();
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, LoginActivity.this);
                }
            });
            if(UserService.getInstance().getCurrentUser()!=null)
            {
                loggedIn();
            }
            Log.d("mycalendar","onCreate: "+UserService.getInstance().getCurrentUser());
            binding.LoginSignUpButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent signUpIntent=new Intent(LoginActivity.this, SignUpActivity.class);
                    startActivity(signUpIntent);
                }
            });

            binding.LoginLoginButton.setOnLongClickListener(new View.OnLongClickListener(){

                @Override
                public boolean onLongClick(View view) {
                    UserService.getInstance().logIn("robert@myemail.com", "robert123", new UserRequestListener() {
                        @Override
                        public void onResponse(boolean success) {
                            if(success){
                                loggedIn();
                            }
                            else{
                                Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, LoginActivity.this);
                    return true;
                }
            });

    }

        private void loggedIn(){
        Intent mainMenuIntent = new Intent(LoginActivity.this, TaskTabActivity.class);
        startActivity(mainMenuIntent);
        finishAffinity();
    }
}