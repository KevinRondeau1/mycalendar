package com.mycalendar.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mycalendar.databinding.RecyclerProjectsBinding;
import com.mycalendar.martha.Project.OnProjectClickListener;
import com.mycalendar.model.Models.Project;

import java.util.ArrayList;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ProjectViewHolder> {

    private final OnProjectClickListener projectClickListener;
    private ArrayList<Project> projects;

    public void setProjects(ArrayList<Project> userProjects) {
        this.projects = userProjects;
    }
    public ArrayList<Project> getProjects() {
        return projects;
    }

    public ProjectsAdapter(OnProjectClickListener projectClickListener) {
        this.projects = new ArrayList<>();
        this.projectClickListener = projectClickListener;
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerProjectsBinding binding = RecyclerProjectsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);


        return new ProjectViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectViewHolder holder, int position) {
        Project project = projects.get(position);

        holder.bind(project);
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }


    public class ProjectViewHolder extends RecyclerView.ViewHolder{
        private RecyclerProjectsBinding binding;

        public ProjectViewHolder(@NonNull RecyclerProjectsBinding binding) {
            super(binding.getRoot());

            this.binding=binding;
        }

        public void bind(final Project project) {

            binding.RecyclerProjectName.setText(project.getProjectName());
            binding.RecyclerProjectDueDate.setText(project.getDueDate());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    projectClickListener.onProjectClicked(project);
                }
            });
        }
    }
}