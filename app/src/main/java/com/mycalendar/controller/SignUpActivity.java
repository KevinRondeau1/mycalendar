package com.mycalendar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mycalendar.controller.tabs.TaskTabActivity;
import com.mycalendar.databinding.ActivitySignupBinding;
import com.mycalendar.martha.User.UserRequestListener;
import com.mycalendar.model.Services.UserService;

public class SignUpActivity extends AppCompatActivity {
    private ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivitySignupBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());

        binding.SignupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String password=binding.SignupTextPassword.getText().toString();
                String passwordConfirm=binding.SignupTextPasswordConfirm.getText().toString();

                if(password.compareTo(passwordConfirm)==0){
                    String email=binding.SignupEmail.getText().toString();
                    String firstname=binding.SignupFirstName.getText().toString();
                    String lastname=binding.SignupLastName.getText().toString();

                    UserService.getInstance().signUp(email, firstname, lastname, password, new UserRequestListener() {
                        @Override
                        public void onResponse(boolean success) {
                            if(success) {
                                Intent signedUp = new Intent(SignUpActivity.this, TaskTabActivity.class);
                                startActivity(signedUp);

                                finishAffinity();
                            } else {
                                Toast.makeText(SignUpActivity.this, "Invalid infos.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, SignUpActivity.this);
                }else{
                    Toast.makeText(SignUpActivity.this,"Password does not match",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}