package com.mycalendar.controller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mycalendar.R;
import com.mycalendar.controller.tabs.ClassesTabActivity;
import com.mycalendar.controller.tabs.TaskTabActivity;
import com.mycalendar.databinding.ActivityClassBinding;
import com.mycalendar.databinding.ActivityTaskBinding;
import com.mycalendar.martha.Class.ClassRequestListener;
import com.mycalendar.martha.Task.TaskRequestListener;
import com.mycalendar.martha.Task.TasksFetchListener;
import com.mycalendar.model.Models.Class;
import com.mycalendar.model.Models.Task;
import com.mycalendar.model.Services.ClassService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.TaskService;
import com.mycalendar.model.Services.UserService;
import com.mycalendar.martha.Task.OnTaskClickListener;

import java.time.LocalDate;
import java.util.ArrayList;

public class ClassActivity extends AppCompatActivity implements OnTaskClickListener {
    private final static int ADD_TASK_REQUEST=1;
    private final static int EDIT_TASK_REQUEST=2;
    public final static String KEY_EDIT_TASK_EXTRA="com.mycalendar";
    private final static int ADD_CLASS_REQUEST=1;
    private final static int EDIT_CLASS_REQUEST=2;
    public final static String KEY_EDIT_CLASS_EXTRA="com.mycalendar";
    private ActivityClassBinding binding;
    private int classId;
    private Class Class;
    private TasksAdapter tasksAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding= ActivityClassBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        tasksAdapter = new TasksAdapter(this);
        binding.ClassRecyclerTask.setAdapter(tasksAdapter);
        binding.ClassRecyclerTask.setHasFixedSize(true);
        binding.ClassRecyclerTask.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.ClassRecyclerTask.setLayoutManager(new LinearLayoutManager(this));


        Bundle extras=getIntent().getExtras();
        if(extras!=null){
            final Class Class = extras.getParcelable((ClassesTabActivity.KEY_EDIT_CLASS_EXTRA));
            classId=Class.getId();
            binding.ClassTextClassName.setText(Class.getClassName());
            binding.ClassTextTeacherName.setText(Class.getTeacherName());

            setTitle(Class.getClassName());
            refresh();
            binding.ClassButtonTaskAdd.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent taskIntent = new Intent(ClassActivity.this, TaskActivity.class);
                    startActivity(taskIntent);
                    finish();
                }
            });

        }else {
            classId=-1;
            binding.textView2.setVisibility(View.GONE);
            binding.ClassButtonTaskAdd.setVisibility(View.GONE);
        }



    }
    private ClassRequestListener classRequestListener = new ClassRequestListener() {
        @Override
        public void onResponse(boolean success) {
            if(success){
                finishOk();
            }else{
                Toast.makeText(ClassActivity.this, "Invalid infos", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void refresh(){
        TaskService.getInstance().getClassTask(ClassService.getInstance().getCurrentClass().getId(), new TasksFetchListener() {
            @Override
            public void onResponse(ArrayList<Task> tasks) {

                if(tasks != null){
                    tasksAdapter.setTasks(tasks);
                    tasksAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(ClassActivity.this, "Could not load tasks", Toast.LENGTH_SHORT).show();
                }
            }
        }, this);

    }
    private void finishOk(){
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainDelete:
                new AlertDialog.Builder(this)
                        .setTitle("Delete " + ClassService.getInstance().getCurrentClass().getClassName())
                        .setMessage("Deleting this class and all related task(s). Continue?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                TaskService.getInstance().deleteAllFromClass(classId,new TaskRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if (success) {
                                            ClassService.getInstance().delete(classId, new ClassRequestListener() {
                                                @Override
                                                public void onResponse(boolean success) {
                                                    if (success) {
                                                        ClassService.getInstance().setCurrentClass(null);
                                                        Intent classIntent;
                                                        classIntent = new Intent(ClassActivity.this, ClassesTabActivity.class);
                                                        startActivity(classIntent);
                                                        finishAffinity();
                                                    } else {
                                                        Toast.makeText(ClassActivity.this, "Invalid deletion.", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }, ClassActivity.this);
                                        } else {
                                            Toast.makeText(ClassActivity.this, "Invalid deletion.", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }, ClassActivity.this);

                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            case R.id.MainSave:
                ClassService.getInstance().edit(
                        ClassService.getInstance().getCurrentClass().getId(),
                        binding.ClassTextClassName.getText().toString(),
                        binding.ClassTextTeacherName.getText().toString(),
                        new ClassRequestListener() {
                            @Override
                            public void onResponse(boolean success) {
                                if(success) {
                                    Class = ClassService.getInstance().getCurrentClass();
                                    finishOk();
                                }
                                else Toast.makeText(ClassActivity.this, "Invalid EDIT request.", Toast.LENGTH_SHORT).show();
                            }
                        }, ClassActivity.this);
                return true;
            case R.id.MainAdd:
                ClassService.getInstance().add(
                        binding.ClassTextClassName.getText().toString(),
                        binding.ClassTextTeacherName.getText().toString(),
                        UserService.getInstance().getCurrentUser().getId(),

                        new ClassRequestListener() {
                            @Override
                            public void onResponse(boolean success) {
                                if (success) {
                                    Class = ClassService.getInstance().getCurrentClass();
                                    Intent classIntent = new Intent(ClassActivity.this, ClassesTabActivity.class);
                                    classIntent.putExtra(ClassActivity.KEY_EDIT_CLASS_EXTRA, Class);
                                    startActivity(classIntent);
                                } else
                                    Toast.makeText(ClassActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                            }
                        }, ClassActivity.this);
                return true;

            default:
                Intent projectIntent = new Intent(ClassActivity.this, ClassesTabActivity.class);
                startActivity(projectIntent);
                return true;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (classId == -1){
            inflater.inflate(R.menu.add_option, menu);
        } else {
            inflater.inflate(R.menu.delete_or_save_options, menu);
        }
        return true;
    }
    @Override
    public void onTaskClicked(Task task) {
        Intent editTaskIntent=new Intent(ClassActivity.this, TaskActivity.class);
        editTaskIntent.putExtra(KEY_EDIT_TASK_EXTRA, task);
        startActivityForResult(editTaskIntent, EDIT_TASK_REQUEST);
        TaskService.getInstance().setCurrentTask(task);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK&& (requestCode==ADD_TASK_REQUEST || requestCode==EDIT_TASK_REQUEST) ){
            refresh();
        }
    }
}
