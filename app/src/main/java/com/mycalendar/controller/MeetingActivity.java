package com.mycalendar.controller;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mycalendar.R;
import com.mycalendar.databinding.ActivityMeetingDetailBinding;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Services.MeetingService;
import com.mycalendar.model.Services.ProjectService;

import java.util.Calendar;

public class MeetingActivity extends AppCompatActivity {
    private Meeting meeting;
    private int meetingId;
    private ActivityMeetingDetailBinding binding;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMeetingDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.MeetingTimePicker.setIs24HourView(true);
        binding.MeetingTimePicker.setHour(0);
        binding.MeetingTimePicker.setMinute(0);
        binding.MeetingDurationPicker.setIs24HourView(true);
        binding.MeetingDurationPicker.setHour(0);
        binding.MeetingDurationPicker.setMinute(0);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            meeting = extras.getParcelable((ProjectActivity.KEY_EDIT_MEETING_EXTRA));

            meetingId=meeting.getId();
            binding.MeetingPlace.setText(meeting.getPlace());
            binding.MeetingTimePicker.setHour(meeting.getMeetHour());
            binding.MeetingTimePicker.setMinute(meeting.getMeetMinute());
            binding.MeetingDurationPicker.setHour(meeting.getDurationHours());
            binding.MeetingDurationPicker.setMinute(meeting.getDurationMinutes());
            binding.MeetingAttendances.setText(meeting.getAttendances());

            /* BEGIN This section is used to set the date */
            date = meeting.getMeetDate();
            String parts[] = date.split("-");

            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            month -= 1;
            int day = Integer.parseInt(parts[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            long milliTime = calendar.getTimeInMillis();
            binding.MeetingCalendarMeetTime.setDate(milliTime, true, true);
            /* END This section is used to set the date */

            setTitle(meeting.getPlace());
        } else {
            meetingId = -1;

            /* BEGIN This section is used to set the date */
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH);
            m += 1;
            date = "" + calendar.get(Calendar.YEAR) + "-" + m + "-" + calendar.get(Calendar.DAY_OF_MONTH);
            /* END This section is used to set the date */

            binding.MeetingPlace.setText("");
            binding.MeetingTimePicker.setHour(0);
            binding.MeetingTimePicker.setMinute(0);
            binding.MeetingDurationPicker.setHour(0);
            binding.MeetingDurationPicker.setMinute(0);
            binding.MeetingAttendances.setText("");
        }

        /* BEGIN This section is used update the selected date */
        binding.MeetingCalendarMeetTime.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month += 1;
                String formattedDay = "0";

                if(dayOfMonth >= 1 && dayOfMonth < 10) formattedDay = "0" + dayOfMonth;
                else formattedDay = Integer.toString(dayOfMonth);

                date = Integer.toString(year) + "-" + Integer.toString(month) + "-" + formattedDay;
                //Toast.makeText(view.getContext(), date, Toast.LENGTH_SHORT).show();
            }
        });
        /* END This section is used update the selected date */

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (meetingId == -1){
            inflater.inflate(R.menu.add_option, menu);
        } else {
            inflater.inflate(R.menu.delete_or_save_options, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainDelete:
                MeetingService.getInstance().delete(meetingId, new MeetingRequestListener() {
                    @Override
                    public void onResponse(boolean success) {
                        if(success) finishOk();
                        else Toast.makeText(MeetingActivity.this, "Invalid", Toast.LENGTH_SHORT).show();
                    }
                }, MeetingActivity.this);
            case R.id.MainSave:
                MeetingService.getInstance().edit(
                        meetingId,
                        binding.MeetingPlace.getText().toString(), date,
                        binding.MeetingTimePicker.getHour(),
                        binding.MeetingTimePicker.getMinute(),
                        binding.MeetingDurationPicker.getHour(),
                        binding.MeetingDurationPicker.getMinute(),
                        binding.MeetingAttendances.getText().toString(),
                        new MeetingRequestListener() {
                            @Override
                            public void onResponse(boolean success) {
                                if(success) {
                                    meeting = MeetingService.getInstance().getCurrentMeeting();
                                    finishOk();
                                }
                                else Toast.makeText(MeetingActivity.this, "Invalid EDIT request.", Toast.LENGTH_SHORT).show();
                            }
                        }, MeetingActivity.this);
            case R.id.MainAdd:
                if(meetingId == -1){
                    MeetingService.getInstance().add(
                            ProjectService.getInstance().getCurrentProject().getId(),
                            binding.MeetingPlace.getText().toString(), date,
                            binding.MeetingTimePicker.getHour(),
                            binding.MeetingTimePicker.getMinute(),
                            binding.MeetingDurationPicker.getHour(),
                            binding.MeetingDurationPicker.getMinute(),
                            binding.MeetingAttendances.getText().toString(),
                            new MeetingRequestListener() {
                                @Override
                                public void onResponse(boolean success) {
                                    if(success) {
                                        meeting = MeetingService.getInstance().getCurrentMeeting();
                                        finishOk();
                                    }
                                    else{ Toast.makeText(MeetingActivity.this, "Invalid ADD request.", Toast.LENGTH_SHORT).show();}
                                }
                            }, MeetingActivity.this);
                }
            default:
                finishOk();
                return true;
        }
    }

    private void finishOk(){
        setResult(RESULT_OK);
        finish();
    }
}
