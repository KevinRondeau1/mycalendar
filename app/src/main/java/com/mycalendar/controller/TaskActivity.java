package com.mycalendar.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;


import com.mycalendar.R;
import com.mycalendar.controller.tabs.ProjectTabActivity;
import com.mycalendar.controller.tabs.TaskTabActivity;
import com.mycalendar.databinding.ActivityTaskBinding;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Task.TaskRequestListener;
import com.mycalendar.model.Models.Project;
import com.mycalendar.model.Models.Task;
import com.mycalendar.model.Services.ClassService;
import com.mycalendar.model.Services.MeetingService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.TaskService;
import com.mycalendar.model.Services.UserService;

import java.util.Calendar;

public class TaskActivity extends AppCompatActivity {
    private String date;
    private int taskId;
    private ActivityTaskBinding binding;
    private Task task;
    private final static int ADD_TASK_REQUEST=1;
    private final static int EDIT_TASK_REQUEST=2;
    public final static String KEY_EDIT_TASK_EXTRA="mycalendar.task";
    public final static String KEY_EDIT_CLASS_EXTRA="com.mycalendar";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding= ActivityTaskBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.TaskTitle.setText("");
        binding.TaskMultiDescription.setText("");
        binding.TaskTextState.setText("");
        binding.TaskNumberCompletion.setText("");

        Bundle extras=getIntent().getExtras();
        if(extras!=null){
            task = TaskService.getInstance().getCurrentTask();
            taskId=task.getId();
            binding.TaskTitle.setText(task.getName());
            binding.TaskMultiDescription.setText(task.getDescription());
            binding.TaskTextState.setText(task.getState());
            binding.TaskNumberCompletion.setText(String.valueOf(task.getCompletion()));
            /* BEGIN This section is used to set the date */
            date = task.getDueDate();
            String parts[] = date.split("-");

            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            month -= 1;
            int day = Integer.parseInt(parts[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            long milliTime = calendar.getTimeInMillis();
            binding.TaskCalendarTime.setDate(milliTime, true, true);
            /* END This section is used to set the date */
            setTitle(task.getName());

        }else {
            task = TaskService.getInstance().getCurrentTask();
            taskId=-1;
            /* BEGIN This section is used to set the date */
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH);
            m += 1;
            date = "" + calendar.get(Calendar.YEAR) + "-" + m + "-" + calendar.get(Calendar.DAY_OF_MONTH);
            /* END This section is used to set the date */
        }
        /* BEGIN This section is used update the selected date */
        binding.TaskCalendarTime.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month += 1;
                String formattedDay = "0";
                if(dayOfMonth >= 1 && dayOfMonth < 10) formattedDay = "0" + dayOfMonth;
                else formattedDay = Integer.toString(dayOfMonth);
                date = Integer.toString(year) + "-" + Integer.toString(month) + "-" + formattedDay;
                //Toast.makeText(view.getContext(), date, Toast.LENGTH_SHORT).show();
            }
        });
        /* END This section is used update the selected date */

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainDelete:
                new AlertDialog.Builder(this)
                        .setTitle("Delete " + task.getName())
                        .setMessage("Deleting this task. Continue?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                TaskService.getInstance().delete(taskId, new TaskRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if(success){
                                            TaskService.getInstance().setCurrentTask(null);
                                            finishOk();
                                        } else {
                                            Toast.makeText(TaskActivity.this, "Invalid deletion.", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }, TaskActivity.this);
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                        return true;
            case R.id.MainSave:
                TaskService.getInstance().edit(
                        taskId,
                        binding.TaskTitle.getText().toString(),
                        binding.TaskMultiDescription.getText().toString(),
                        Integer.parseInt(binding.TaskNumberCompletion.getText().toString()),
                        binding.TaskTextState.getText().toString(),
                        date,
                        new TaskRequestListener() {
                            @Override
                            public void onResponse(boolean success) {
                                if(success) {
                                    task = TaskService.getInstance().getCurrentTask();
                                    finishOk();
                                }
                                else Toast.makeText(TaskActivity.this, "Invalid EDIT request. Be sure to fill all fields and/or respect the parameters", Toast.LENGTH_SHORT).show();
                            }
                        }, TaskActivity.this);
                return true;
            case R.id.MainAdd:
                if(!binding.TaskNumberCompletion.getText().toString().isEmpty()) {
                    if (ProjectService.getInstance().getCurrentProject() != null) {
                        TaskService.getInstance().addFromProject(
                                binding.TaskTitle.getText().toString(),
                                binding.TaskMultiDescription.getText().toString(),
                                Integer.parseInt(binding.TaskNumberCompletion.getText().toString()),
                                binding.TaskTextState.getText().toString(),
                                date,
                                UserService.getInstance().getCurrentUser().getId(),
                                ProjectService.getInstance().getCurrentProject().getId(),

                                new TaskRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if (success) {
                                            task = TaskService.getInstance().getCurrentTask();
                                            Intent taskIntent = new Intent(TaskActivity.this, ProjectActivity.class);
                                            taskIntent.putExtra(ProjectActivity.KEY_RECEIVE_PROJECT_EXTRA, ProjectService.getInstance().getCurrentProject());
                                            startActivity(taskIntent);
                                            finishOk();
                                        } else
                                            Toast.makeText(TaskActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                                    }
                                }, TaskActivity.this);

                    } else if (ClassService.getInstance().getCurrentClass() != null) {
                        TaskService.getInstance().addFromClass(
                                binding.TaskTitle.getText().toString(),
                                binding.TaskMultiDescription.getText().toString(),
                                Integer.parseInt(binding.TaskNumberCompletion.getText().toString()),
                                binding.TaskTextState.getText().toString(),
                                date,
                                UserService.getInstance().getCurrentUser().getId(),
                                ClassService.getInstance().getCurrentClass().getId(),

                                new TaskRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if (success) {
                                            Intent classIntent = new Intent(TaskActivity.this, ClassActivity.class);
                                            classIntent.putExtra(ClassActivity.KEY_EDIT_TASK_EXTRA, ClassService.getInstance().getCurrentClass());
                                            startActivity(classIntent);
                                            finishOk();
                                        } else
                                            Toast.makeText(TaskActivity.this, "Invalid information. Be sure to fill all fields and/or respect the parameters", Toast.LENGTH_SHORT).show();
                                    }
                                }, TaskActivity.this);
                    } else {
                        TaskService.getInstance().add(
                                binding.TaskTitle.getText().toString(),
                                binding.TaskMultiDescription.getText().toString(),
                                Integer.parseInt(binding.TaskNumberCompletion.getText().toString()),
                                binding.TaskTextState.getText().toString(),
                                date,
                                UserService.getInstance().getCurrentUser().getId(),

                                new TaskRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if (success) {
                                            task = TaskService.getInstance().getCurrentTask();
                                            Intent taskIntent = new Intent(TaskActivity.this, TaskTabActivity.class);
                                            taskIntent.putExtra(ProjectActivity.KEY_EDIT_TASK_EXTRA, task);
                                            startActivity(taskIntent);
                                            finishOk();
                                        } else
                                            Toast.makeText(TaskActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                                    }
                                }, TaskActivity.this);
                    }
                }else
                    Toast.makeText(TaskActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                return true;

            default:
                Intent projectIntent = new Intent(TaskActivity.this, TaskTabActivity.class);
                startActivity(projectIntent);
                return true;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (taskId == -1){
            inflater.inflate(R.menu.add_option, menu);
        } else {
            inflater.inflate(R.menu.delete_or_save_options, menu);
        }
        return true;
    }

    private TaskRequestListener taskRequestListener = new TaskRequestListener() {
        @Override
        public void onResponse(boolean success) {
            if(success){
                finishOk();
            }else{
                Toast.makeText(TaskActivity.this, "Invalid infos", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void finishOk(){
        setResult(RESULT_OK);
        finish();
    }

}