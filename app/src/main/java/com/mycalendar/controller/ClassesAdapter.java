package com.mycalendar.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mycalendar.databinding.RecyclerClassesBinding;
import com.mycalendar.martha.Class.OnClassClickListener;
import com.mycalendar.model.Models.Class;
import java.util.ArrayList;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.ClassViewHolder> {

    private final OnClassClickListener classClickListener;
    private ArrayList<Class> classes;


    public void setClasses(ArrayList<Class> userClasses) {
        this.classes = userClasses;
    }

    public ClassesAdapter(OnClassClickListener classClickListener) {
        this.classes = new ArrayList<>();
        this.classClickListener = classClickListener;
    }

    @NonNull
    @Override
    public ClassViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerClassesBinding binding = RecyclerClassesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);


        return new ClassViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassViewHolder holder, int position) {
        Class Class = classes.get(position);

        holder.bind(Class);
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }


    public class ClassViewHolder extends RecyclerView.ViewHolder{
        private RecyclerClassesBinding binding;

        public ClassViewHolder(@NonNull RecyclerClassesBinding binding) {
            super(binding.getRoot());

            this.binding=binding;
        }

        public void bind(final Class Class) {

            binding.RecyclerClassName.setText(Class.getClassName());
            binding.ReyclerClassTeacher.setText(Class.getTeacherName());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    classClickListener.onClassClicked(Class);
                }
            });
        }
    }
}