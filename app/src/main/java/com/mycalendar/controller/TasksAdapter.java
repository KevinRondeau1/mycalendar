package com.mycalendar.controller;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mycalendar.databinding.RecyclerTasksBinding;
import com.mycalendar.martha.Task.OnTaskClickListener;
import com.mycalendar.model.Models.Task;
import java.util.ArrayList;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TaskViewHolder> {

    private final OnTaskClickListener taskClickListener;
    private ArrayList<Task> tasks;


    public void setTasks(ArrayList<Task> userTasks) {
        this.tasks = userTasks;
    }

    public TasksAdapter(OnTaskClickListener taskClickListener) {
        this.tasks = new ArrayList<>();
        this.taskClickListener = taskClickListener;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerTasksBinding binding = RecyclerTasksBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);


        return new TaskViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        Task task = tasks.get(position);

        holder.bind(task);
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }


    public class TaskViewHolder extends RecyclerView.ViewHolder{
        private RecyclerTasksBinding binding;

        public TaskViewHolder(@NonNull RecyclerTasksBinding binding) {
            super(binding.getRoot());

            this.binding=binding;
        }

        public void bind(final Task task) {

            binding.RecyclerTaskText.setText(task.getName());
            binding.RecyclerTaskProgress.setProgress(task.getCompletion());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    taskClickListener.onTaskClicked(task);
                }
            });
        }
    }
}