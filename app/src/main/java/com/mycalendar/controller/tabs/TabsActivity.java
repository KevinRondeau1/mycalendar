package com.mycalendar.controller.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.tabs.TabLayout;
import com.mycalendar.R;
import com.mycalendar.controller.LoginActivity;
import com.mycalendar.controller.MeetingActivity;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.model.Services.MeetingService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.UserService;


public abstract class TabsActivity extends AppCompatActivity {
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = findViewById(R.id.tabsNav);
        tabLayout = (TabLayout)findViewById(R.id.tabsNav);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() != getPosition()) {
                    // Appliquer seulement si la selection est differente, sinon dans onStart on crée une boucle infinie

                    Class[] activities = { TaskTabActivity.class, ClassesTabActivity.class, ProjectTabActivity.class };

                    Intent intent = new Intent(TabsActivity.this, activities[tab.getPosition()]);
                    startActivity(intent);
                    overridePendingTransition(0, 0);


                    finish();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {  }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {  }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        tabLayout.getTabAt(getPosition()).select();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainLogout:
                UserService.getInstance().logOut();
                Intent logoutIntent = new Intent(TabsActivity.this, LoginActivity.class);
                startActivity(logoutIntent);
                return true;
            default:
                return true;
        }
    }

    public abstract int getPosition();
}