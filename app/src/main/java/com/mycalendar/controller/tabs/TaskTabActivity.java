package com.mycalendar.controller.tabs;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mycalendar.controller.DetailProjectActivity;
import com.mycalendar.controller.LoginActivity;
import com.mycalendar.controller.TaskActivity;
import com.mycalendar.martha.Task.OnTaskClickListener;
import com.mycalendar.controller.TasksAdapter;
import com.mycalendar.databinding.ActivityTaskTabBinding;
import com.mycalendar.martha.Task.TasksFetchListener;
import com.mycalendar.model.Models.Task;
import com.mycalendar.model.Services.ClassService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.TaskService;
import com.mycalendar.model.Services.UserService;
import java.util.ArrayList;

public class TaskTabActivity extends TabsActivity implements OnTaskClickListener {

    private final static int ADD_TASK_REQUEST=1;
    private final static int EDIT_TASK_REQUEST=2;
    public final static String KEY_EDIT_TASK_EXTRA="com.mycalendar";
    private ActivityTaskTabBinding binding;

    @Override
    public int getPosition() {
        return 0;
    }

    private TasksAdapter tasksAdapter;
    private int getCurrentUserId() {
        return UserService.getInstance().getCurrentUser().getId();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityTaskTabBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        super.onCreate(savedInstanceState);

        tasksAdapter = new TasksAdapter(this);
        binding.RecyclerTask.setAdapter(tasksAdapter);
        binding.RecyclerTask.setHasFixedSize(true);
        binding.RecyclerTask.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.RecyclerTask.setLayoutManager(new LinearLayoutManager(this));
        ClassService.getInstance().setCurrentClass(null);
        ProjectService.getInstance().setCurrentProject(null);
        refresh();
        setTitle("Tasks");

        binding.TaskFabAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent taskAddIntent = new Intent(TaskTabActivity.this, TaskActivity.class);
                startActivity(taskAddIntent);
            }

        });

    }
    private void refresh(){
        TaskService.getInstance().getAll(getCurrentUserId(), new TasksFetchListener() {
            @Override
            public void onResponse(ArrayList<Task> tasks) {

                if(tasks != null){
                    tasksAdapter.setTasks(tasks);
                    tasksAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(TaskTabActivity.this, "Could not load tasks", Toast.LENGTH_SHORT).show();
                }
            }
        }, this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK&& (requestCode==ADD_TASK_REQUEST || requestCode==EDIT_TASK_REQUEST) ){
            refresh();
        }
    }

    @Override
    public void onTaskClicked(Task task) {
        Intent editTaskIntent=new Intent(TaskTabActivity.this, TaskActivity.class);
        editTaskIntent.putExtra(KEY_EDIT_TASK_EXTRA, task);
        startActivityForResult(editTaskIntent, EDIT_TASK_REQUEST);
        TaskService.getInstance().setCurrentTask(task);
    }

}