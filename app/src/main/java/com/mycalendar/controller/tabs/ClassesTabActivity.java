package com.mycalendar.controller.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mycalendar.controller.DetailProjectActivity;
import com.mycalendar.controller.LoginActivity;
import com.mycalendar.controller.ClassActivity;
import com.mycalendar.martha.Class.OnClassClickListener;
import com.mycalendar.controller.ClassesAdapter;
import com.mycalendar.databinding.ActivityClassesTabBinding;
import com.mycalendar.martha.Class.ClassesFetchListener;
import com.mycalendar.model.Models.Class;
import com.mycalendar.model.Services.ClassService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.UserService;

import java.util.ArrayList;

public class ClassesTabActivity extends TabsActivity implements OnClassClickListener{
    private final static int ADD_CLASS_REQUEST=1;
    private final static int EDIT_CLASS_REQUEST=2;
    public final static String KEY_EDIT_CLASS_EXTRA="com.mycalendar";
    private final static int ADD_TASK_REQUEST=1;
    private final static int EDIT_TASK_REQUEST=2;
    public final static String KEY_EDIT_TASK_EXTRA="com.mycalendar";
    private ActivityClassesTabBinding binding;

    @Override
    public int getPosition() {
        return 1;
    }

    private ClassesAdapter classesAdapter;
    private int getCurrentUserId() {
        return UserService.getInstance().getCurrentUser().getId();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityClassesTabBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        super.onCreate(savedInstanceState);

        classesAdapter=new ClassesAdapter(this);
        binding.RecyclerClass.setAdapter(classesAdapter);
        binding.RecyclerClass.setHasFixedSize(true);
        binding.RecyclerClass.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.RecyclerClass.setLayoutManager(new LinearLayoutManager(this));

        refresh();
        setTitle("Classes");
        ClassService.getInstance().setCurrentClass(null);
        ProjectService.getInstance().setCurrentProject(null);

        binding.ClassFabAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent classAddIntent = new Intent(ClassesTabActivity.this,ClassActivity.class);
                startActivity(classAddIntent);
            }

        });

    }
    private void refresh(){
        ClassService.getInstance().getAll(getCurrentUserId(), new ClassesFetchListener() {
            @Override
            public void onResponse(ArrayList<Class> classes) {
                if(classes != null){
                    classesAdapter.setClasses(classes);
                    classesAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(ClassesTabActivity.this, "Could not load classes", Toast.LENGTH_SHORT).show();
                }
            }
        }, this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK&& (requestCode==ADD_CLASS_REQUEST || requestCode==EDIT_CLASS_REQUEST) ){
            refresh();
        }
        if(resultCode==RESULT_OK&& (requestCode==ADD_TASK_REQUEST || requestCode==EDIT_TASK_REQUEST) ){
            refresh();
        }
    }
    @Override
    public void onClassClicked(Class Class) {
        Intent editClassIntent=new Intent(ClassesTabActivity.this, ClassActivity.class);
        editClassIntent.putExtra(KEY_EDIT_CLASS_EXTRA, Class);
        startActivityForResult(editClassIntent, EDIT_CLASS_REQUEST);
        ClassService.getInstance().setCurrentClass(Class);
    }
}