package com.mycalendar.controller.tabs;

import com.mycalendar.controller.DetailProjectActivity;
import com.mycalendar.controller.LoginActivity;
import com.mycalendar.controller.MeetingActivity;
import com.mycalendar.controller.ProjectActivity;
import com.mycalendar.controller.ProjectsAdapter;
import com.mycalendar.databinding.ActivityProjectTabBinding;
import com.mycalendar.martha.Project.OnProjectClickListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Project.ProjectsFetchListener;
import com.mycalendar.model.Models.Project;
import com.mycalendar.model.Services.ClassService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.UserService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

public class ProjectTabActivity extends TabsActivity implements OnProjectClickListener {

    private final static int ADD_PROJECT_REQUEST=1;
    private final static int EDIT_PROJECT_REQUEST=2;
    public final static String KEY_EDIT_PROJECT_EXTRA="com.mycalendar";
    private ActivityProjectTabBinding binding;
    private int userId;

    @Override
    public int getPosition() {
        return 2;
    }

    private ProjectsAdapter projectsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityProjectTabBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        super.onCreate(savedInstanceState);

        userId = UserService.getInstance().getCurrentUser().getId();

        projectsAdapter = new ProjectsAdapter(this);
        binding.RecyclerProject.setAdapter(projectsAdapter);
        binding.RecyclerProject.setHasFixedSize(true);
        binding.RecyclerProject.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.RecyclerProject.setLayoutManager(new LinearLayoutManager(this));

        refresh();
        setTitle("Projects");
        ClassService.getInstance().setCurrentClass(null);
        ProjectService.getInstance().setCurrentProject(null);

        /* Setting buttons actions */
        binding.ProjectFabAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent projectAddIntent = new Intent(ProjectTabActivity.this, DetailProjectActivity.class);
                startActivity(projectAddIntent);
            }

        });
    }

    /******************************** FUNCTIONS OUT OF onCreate() ********************************/

    private void refresh(){
        ProjectService.getInstance().getAll(userId, new ProjectsFetchListener() {
            @Override
            public void onResponse(ArrayList<Project> projects) {

                if(projects != null){
                    projectsAdapter.setProjects(projects);
                    projectsAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(ProjectTabActivity.this, "Could not load projects", Toast.LENGTH_SHORT).show();
                }
            }
        }, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && (requestCode == ADD_PROJECT_REQUEST || requestCode == EDIT_PROJECT_REQUEST) ){
            refresh();
        }
    }

    @Override
    public void onProjectClicked(Project project) {
        Intent editProjectIntent=new Intent(ProjectTabActivity.this, ProjectActivity.class);
        editProjectIntent.putExtra(ProjectActivity.KEY_RECEIVE_PROJECT_EXTRA, project);
        startActivityForResult(editProjectIntent, EDIT_PROJECT_REQUEST);
        ProjectService.getInstance().setCurrentProject(project);
    }
}