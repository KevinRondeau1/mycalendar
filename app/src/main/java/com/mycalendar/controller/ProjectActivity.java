package com.mycalendar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mycalendar.R;
import com.mycalendar.controller.tabs.ClassesTabActivity;
import com.mycalendar.controller.tabs.ProjectTabActivity;
import com.mycalendar.controller.tabs.TaskTabActivity;
import com.mycalendar.databinding.ActivityProjectBinding;
import com.mycalendar.databinding.ActivityProjectDetailBinding;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Meeting.MeetingsFetchListener;
import com.mycalendar.martha.Meeting.OnMeetingClickListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Task.OnTaskClickListener;
import com.mycalendar.martha.Task.TasksFetchListener;
import com.mycalendar.martha.User.UserRequestListener;
import com.mycalendar.model.Models.Meeting;
import com.mycalendar.model.Models.Project;
import com.mycalendar.model.Models.Task;
import com.mycalendar.model.Models.User;
import com.mycalendar.model.Services.MeetingService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.TaskService;
import com.mycalendar.model.Services.UserService;

import java.util.ArrayList;

public class ProjectActivity extends AppCompatActivity implements OnTaskClickListener, OnMeetingClickListener {
    public final static int ADD_MEETING_REQUEST=10;
    public final static int EDIT_MEETING_REQUEST=11;
    public final static String KEY_EDIT_MEETING_EXTRA="mycalendar.meeting";

    public final static int ADD_PROJECT_REQUEST=20;
    public final static int EDIT_PROJECT_REQUEST=21;
    public final static String KEY_EDIT_PROJECT_EXTRA="mycalendar.project";

    public final static int ADD_TASK_REQUEST=30;
    public final static int EDIT_TASK_REQUEST=31;
    public final static String KEY_EDIT_TASK_EXTRA="mycalendar.task";

    public final static String KEY_RECEIVE_PROJECT_EXTRA="com.example.receive.project";

    Project project;
    private int projectId;
    private MeetingsAdapter meetingsAdapter;
    private TasksAdapter tasksAdapter;
    private ActivityProjectBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityProjectBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        super.onCreate(savedInstanceState);

        binding.ProjectRecyclerMeeting.setHasFixedSize(true);
        binding.ProjectRecyclerMeeting.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.ProjectRecyclerMeeting.setLayoutManager(new LinearLayoutManager(this));
        meetingsAdapter = new MeetingsAdapter(this);
        binding.ProjectRecyclerMeeting.setAdapter(meetingsAdapter);

        binding.ProjectRecyclerTask.setHasFixedSize(true);
        binding.ProjectRecyclerTask.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        binding.ProjectRecyclerTask.setLayoutManager(new LinearLayoutManager(this));
        tasksAdapter = new TasksAdapter(this);
        binding.ProjectRecyclerTask.setAdapter(tasksAdapter);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            project = extras.getParcelable((KEY_RECEIVE_PROJECT_EXTRA));
            ProjectService.getInstance().setCurrentProject(project);
        }
        else project = ProjectService.getInstance().getCurrentProject();

        projectId = project.getId();
        refresh();

        /* Setting buttons actions */
        binding.ProjectMeetingAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent meetingIntent = new Intent(ProjectActivity.this, MeetingActivity.class);
                startActivityForResult(meetingIntent, ADD_MEETING_REQUEST);
            }
        });

        binding.ProjectTaskAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent meetingIntent = new Intent(ProjectActivity.this, TaskActivity.class);
                startActivity(meetingIntent);
            }
        });
    }

    /******************************** FUNCTIONS OUT OF onCreate() ********************************/

    private void refresh(){
        project = ProjectService.getInstance().getCurrentProject();
        setTitle(project.getProjectName());
        refreshMeetings();
        refreshTasks();
    }

    private void refreshMeetings(){
        MeetingService.getInstance().getAll(projectId, new MeetingsFetchListener() {
            @Override
            public void onResponse(ArrayList<Meeting> meetings) {

                if(meetings != null){
                    meetingsAdapter.setMeetings(meetings);
                    meetingsAdapter.notifyDataSetChanged();
                }
                else Toast.makeText(ProjectActivity.this, "Could not load meetings", Toast.LENGTH_SHORT).show();
            }
        }, ProjectActivity.this);
    }

    private void refreshTasks(){
        TaskService.getInstance().getProjectTask(projectId, new TasksFetchListener() {
            @Override
            public void onResponse(ArrayList<Task> tasks) {
                if(tasks != null){
                    tasksAdapter.setTasks(tasks);
                    tasksAdapter.notifyDataSetChanged();
                }
                else Toast.makeText(ProjectActivity.this, "Could not load tasks", Toast.LENGTH_SHORT).show();
            }
        }, ProjectActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && (requestCode == ADD_MEETING_REQUEST || requestCode == EDIT_MEETING_REQUEST) ){
            refreshMeetings();
        }
        if(resultCode == RESULT_OK && (requestCode == ADD_TASK_REQUEST || requestCode == EDIT_TASK_REQUEST) ){
            refreshTasks();
        }
        if(resultCode == RESULT_OK && (requestCode == ADD_PROJECT_REQUEST || requestCode == EDIT_PROJECT_REQUEST) ){
            refresh();
        }
    }

    @Override
    public void onMeetingClicked(Meeting meeting) {
        Intent editMeetingIntent=new Intent(ProjectActivity.this, MeetingActivity.class);
        editMeetingIntent.putExtra(KEY_EDIT_MEETING_EXTRA, meeting);
        startActivityForResult(editMeetingIntent, EDIT_MEETING_REQUEST);
        MeetingService.getInstance().setCurrentMeeting(meeting);
    }

    @Override
    public void onTaskClicked(Task task) {
        Intent editTaskIntent=new Intent(ProjectActivity.this, TaskActivity.class);
        editTaskIntent.putExtra(KEY_EDIT_TASK_EXTRA, task);
        startActivityForResult(editTaskIntent, EDIT_TASK_REQUEST);
        TaskService.getInstance().setCurrentTask(task);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_option, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainEdit:
                Intent detailIntent = new Intent(ProjectActivity.this, DetailProjectActivity.class);
                detailIntent.putExtra(KEY_EDIT_PROJECT_EXTRA, project);
                startActivityForResult(detailIntent, EDIT_PROJECT_REQUEST);
                return true;
            default:
                Intent projectIntent = new Intent(ProjectActivity.this, ProjectTabActivity.class);
                startActivity(projectIntent);
                return true;
        }
    }

    private void finishOk(){
        setResult(RESULT_OK);
        finish();
    }

}
