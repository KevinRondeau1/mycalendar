package com.mycalendar.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;

import com.mycalendar.R;
import com.mycalendar.controller.tabs.ProjectTabActivity;
import com.mycalendar.databinding.ActivityProjectDetailBinding;
import com.mycalendar.martha.Meeting.MeetingRequestListener;
import com.mycalendar.martha.Project.ProjectRequestListener;
import com.mycalendar.martha.Task.TaskRequestListener;
import com.mycalendar.model.Models.Project;
import com.mycalendar.model.Models.Task;
import com.mycalendar.model.Services.MeetingService;
import com.mycalendar.model.Services.ProjectService;
import com.mycalendar.model.Services.TaskService;

import java.util.Calendar;

public class DetailProjectActivity extends AppCompatActivity {
    private int projectId;
    private Project project;
    private ActivityProjectDetailBinding binding;
    private String date;

    private MenuItem forceDelete;
    private View forceDeleteView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityProjectDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle extras=getIntent().getExtras();
        if(extras != null){
            project = extras.getParcelable((ProjectActivity.KEY_EDIT_PROJECT_EXTRA));
            projectId=project.getId();
            binding.ProjectProjectTitle.setText(project.getProjectName());
            binding.ProjectTeamMembersMultiline.setText(project.getTeamMembers());
            binding.ProjectNoteMultiline.setText(project.getNote());
            setTitle(project.getProjectName());

            /* BEGIN This section is used to set the date */
            date = project.getDueDate();
            String parts[] = date.split("-");

            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            month -= 1;
            int day = Integer.parseInt(parts[2]);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            long milliTime = calendar.getTimeInMillis();
            binding.ProjectCalendarDueDate.setDate(milliTime, true, true);
            /* END This section is used to set the date */
        } else {
            project = ProjectService.getInstance().getCurrentProject();
            projectId = -1;

            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH);
            m += 1;
            date = "" + calendar.get(Calendar.YEAR) + "-" + m + "-" + calendar.get(Calendar.DAY_OF_MONTH);
        }

        /* BEGIN This section is used update the selected date */
        binding.ProjectCalendarDueDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month += 1;
                String formattedDay = "0";

                if(dayOfMonth >= 1 && dayOfMonth < 10) formattedDay = "0" + dayOfMonth;
                else formattedDay = Integer.toString(dayOfMonth);

                date = Integer.toString(year) + "-" + Integer.toString(month) + "-" + formattedDay;
            }
        });
        /* END This section is used update the selected date */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (projectId == -1){
            inflater.inflate(R.menu.add_option, menu);
        } else {
            inflater.inflate(R.menu.delete_or_save_options, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.MainDelete:
                new AlertDialog.Builder(this)
                        .setTitle("Delete " + project.getProjectName())
                        .setMessage("Deleting this project will delete all its associated meeting(s) and task(s). Continue?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                MeetingService.getInstance().deleteAll(projectId, new MeetingRequestListener() {
                                    @Override
                                    public void onResponse(boolean success) {
                                        if(success){
                                            TaskService.getInstance().deleteAll(projectId, new TaskRequestListener() {
                                                @Override
                                                public void onResponse(boolean success) {
                                                    if(success){
                                                        ProjectService.getInstance().delete(projectId, new ProjectRequestListener() {
                                                            @Override
                                                            public void onResponse(boolean success) {
                                                                if(success){
                                                                    ProjectService.getInstance().setCurrentProject(null);
                                                                    Intent projectIntent = new Intent(DetailProjectActivity.this, ProjectTabActivity.class);
                                                                    startActivity(projectIntent);
                                                                    finishAffinity();
                                                                } else {
                                                                    Toast.makeText(DetailProjectActivity.this, "Invalid deletion. You must delete all tasks and meetings the project has.", Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }, DetailProjectActivity.this);
                                                    } else {
                                                        Toast.makeText(DetailProjectActivity.this, "There was an error while trying to delete the tasks.", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }, DetailProjectActivity.this);
                                        } else {
                                            Toast.makeText(DetailProjectActivity.this, "There was an error while trying to delete the meetings.", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }, DetailProjectActivity.this);
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            case R.id.MainSave:
                ProjectService.getInstance().edit(projectId,
                        binding.ProjectProjectTitle.getText().toString(),
                        binding.ProjectTeamMembersMultiline.getText().toString(),
                        binding.ProjectNoteMultiline.getText().toString(), date,
                        new ProjectRequestListener() {
                            @Override
                            public void onResponse(boolean success) {
                                if (success) {
                                    project = ProjectService.getInstance().getCurrentProject();
                                    finishOk();
                                } else Toast.makeText(DetailProjectActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                            }
                        }, DetailProjectActivity.this);
                return true;
            case R.id.MainAdd:
                if(projectId == -1){
                    ProjectService.getInstance().add(
                            binding.ProjectProjectTitle.getText().toString(),
                            binding.ProjectTeamMembersMultiline.getText().toString(),
                            binding.ProjectNoteMultiline.getText().toString(), date,
                            new ProjectRequestListener() {
                                @Override
                                public void onResponse(boolean success) {
                                    if (success) {
                                        project = ProjectService.getInstance().getCurrentProject();
                                        Intent projectIntent = new Intent(DetailProjectActivity.this, ProjectActivity.class);
                                        projectIntent.putExtra(ProjectActivity.KEY_RECEIVE_PROJECT_EXTRA, project);
                                        startActivity(projectIntent);
                                        finish();
                                    } else Toast.makeText(DetailProjectActivity.this, "Invalid information. Be sure to fill all fields.", Toast.LENGTH_SHORT).show();
                                }
                            }, DetailProjectActivity.this);
                    return true;
                }
            default:
                Intent projectIntent = new Intent(DetailProjectActivity.this, ProjectTabActivity.class);
                startActivity(projectIntent);
                return true;
        }
    }

    private void finishOk(){
        setResult(RESULT_OK);
        finish();
    }
}